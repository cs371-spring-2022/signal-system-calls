#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{

	if(argc!=2){
		printf("usage: %s pid\n", argv[0]);
		return 1;
	}

	kill(atoi(argv[1]),SIGINT);

	return 0;
}
