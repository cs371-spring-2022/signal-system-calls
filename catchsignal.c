#include <stdio.h>
#include <unistd.h>
#include <signal.h>

void sigint_handler()
{
	printf("I got a SIGINT signal\n");

}



int main(void)
{
	signal(SIGINT, sigint_handler);


	int i = 0;

        while(1){
                printf("%d\n",i);
                i++;
                sleep(1);
        }
	return 0;
}
