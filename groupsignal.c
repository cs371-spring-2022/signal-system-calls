#include <sys/types.h>
#include <errno.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char **argv)
{

       // int status;
        pid_t mypid;
        pid_t result= fork();

        if(result>0){
                mypid = getpid();
                printf("PARENT: I am %d, the parent of (child_id = %d)\n", mypid, result);
		sleep(10);
		kill(0,SIGINT);
        }
        else if  (result == 0) {
		while(1){
	                printf("CHILD: I am %d, the child of %d\n", getpid(), getppid());
			sleep(1);
		}
		
        }
        else{   
                fprintf(stderr, "fork() failed: %s\n", strerror(errno));
                return 1;
        }

        return 0;
}


