#include<stdio.h>
#include<stdlib.h>
#include<signal.h>
#include <unistd.h>

FILE *myfile;

void clean_up(){
	fclose(myfile);
	unlink("temp.dat");
	exit(1);
}


int main(){

	myfile=fopen("temp.dat", "w");
	signal(SIGSEGV, clean_up);
	
	//do some work here
	sleep(5);
	
	int *ptr=NULL;
	printf("%d\n", *ptr);

	return 0;
}
