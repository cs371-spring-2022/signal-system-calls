#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <stdlib.h>

int i;
int no_alarm=0;
void alarm_handler(){
	i = 10;
	printf("SIGALRM received, default number is %d\n", i);
	return;
}

int main(void)
{
	signal(SIGALRM, alarm_handler);
        alarm(5);
	printf("Please entered a number: ");
	while(!no_alarm){
		scanf("%d",&i);
		break;
	}

	return 0;
}
